#!/usr/bin/env python
# encoding: utf-8

"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: Grammar_Test_Day05.py
@time: 2017/12/3 13:33
"""

# region 序列化(变量从内存中变成可存储或传输的过程称之为序列化)
# import pickle

# try:
#     path = 'D:\Project\Project_Git\Python_test\python_test\Comm\dump.txt'
#     d = dict(name='Bob', age=20, score=88)
#     p = pickle.dumps(d)  # 将内存的东西实转为二进制
#     print(p)
#     f = open('D:\Project\Project_Git\Python_test\python_test\Comm\dump.txt', 'wb')
#     pickle.dump(p, f)  # 向文件写入二进制数据
#     # f.write(p)  #向文件写入二进制数据
#     f.close()
#     f = open('D:\Project\Project_Git\Python_test\python_test\Comm\dump.txt', 'rb')
#     g = pickle.load(f)
#     f.close()
#     print(g)
# except BaseException as ex:
#     print('Error', ex)
# finally:
#     print('END')
# endregion

# region Json 转换
# import json
#
#
# # try:
# #     d = dict(name='zhangsna', age=14, score=20)
# #     js = json.dumps(d)  # 返回标准JSon格式str
# #     print(js)
# #     f = open('D:\Project\Project_Git\Python_test\python_test\Comm\\test.txt', 'w+')
# #     json.dump(js, f) #将json格式的数据写入文件
# #     f.close()
# #     f = open('D:\Project\Project_Git\Python_test\python_test\Comm\\test.txt', 'r+')
# #     js = json.load(f) #从文件中将Json格式数据读入到内存
# #     string =json.loads(js) #将json 格式反序列化
# #     print(js)
# #     print(string)
# # except BaseException as e:
# #     print('Error:', e)
# # finally:
# #     print('End')
# class Student(object):
#     def __init__(self, name, age, score):
#         self.name = name
#         self.age = age
#         self.score = score
#
#     def student2dict(self):
#         return {
#             'name': self.name,
#             'age': self.age,
#             'score': self.score
#         }
#
#
# def dic2student(d):
#     return Student(d['name'], d['age'], d['score'])
#
#
# try:
#     stuBob = Student('bob', 15, 90)
#     # js = json.dumps(stuBob.student2dict())
#     js = json.dumps(stuBob, default=lambda obj: obj.__dict__)
#     print(js)
#     print(json.loads(js, object_hook=dic2student))
#
# except BaseException as e:
#     print('Error:', e)
# finally:
#     print('End')

# endregion

# region  线程 （TODO）

# endregion

# region 正则表达式
# import re
#
#
# def matchste(regular, source):
#     m = re.match(regular, source)
#     if (m):
#         print('OK')
#         return m.group()
#     else:
#         print('failed')
#         return None
#
#
# try:
#     s = matchste('00\d', '007')
#     print(s)
# except BaseException as e:
#     print('Error:', e)
# finally:
#     print('End')

# endregion

#region 图形界面 (Tkinter库)（同时还支持wxWidgets,Qt ,GTK ,Win ,java Swing）

#endregion
