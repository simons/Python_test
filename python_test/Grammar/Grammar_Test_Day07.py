#!/usr/bin/env python
# encoding: utf-8

"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: Grammar_Test_Day07.py
@time: 2017/12/6 9:45
"""

# import sys
# sys.path.append('D:\Project\Project_Git\Python_test\python_test\Comm')
# print(sys.path)

# region Web 开发

# # # 从wsgiref模块导入:
# from wsgiref.simple_server import make_server
# from hello import application
#
# # 创建一个服务器，IP地址为空，端口是8000，处理函数是application:
# httpd = make_server('', 8000, application)
# print('Serving HTTP on port 8000...')
# # 开始监听HTTP请求:
# httpd.serve_forever()

# endregion

# region 协程
# def consumer():
#     r = ''
#     while True:
#         n = yield r
#         if not n:
#             return
#         print('[CONSUMER] Consuming %s...' % n)
#         r = '200 OK'
#
#
# def produce(c):
#     c.send(None)
#     n = 0
#     while n < 5:
#         n = n + 1
#         print('[PRODUCER] Producing %s...' % n)
#         r = c.send(n)
#         print('[PRODUCER] Consumer return: %s' % r)
#     c.close()
#
#
# c = consumer()
# produce(c)
# endregion

# region  asyncio
# import asyncio
# import threading
#
#
# @asyncio.coroutine
# def hello():
#     print('Hello world! (%s)' % threading.currentThread())
#     r = yield from asyncio.sleep(1)
#     print(r)
#     print('Hello again! (%s)' % threading.currentThread())
#
#
# loop = asyncio.get_event_loop()
# task = [hello(), hello(), hello()]
# loop.run_until_complete(asyncio.wait(task))
# loop.close()

# endregion

# region async await
# import asyncio
# import threading
#
#
# async def hello():
#     print('Hello world! (%s)' % threading.currentThread())
#     r = asyncio.sleep(1)
#     print(r)
#     print('Hello again! (%s)' % threading.currentThread())
#
#
# loop = asyncio.get_event_loop()
# task = [hello(), hello(), hello()]
# loop.run_until_complete(asyncio.wait(task))
# loop.close()
# endregion

import pip
from subprocess import call

for dist in pip.get_installed_distributions():
    call("sudo pip install --upgrade " + dist.project_name, shell=True)
