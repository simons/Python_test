# region 函数式编程 (functional programming)
# f=abs
# print(abs)
# print(f)
# print(f(-20))
# import  math
# import builtins
# print(max(3,2))
# def max(x, y, f):
#     return f(x) + f(y)
# print(max(3,-2,abs))
# print(builtins.max(3,2))
# endregion

# region Map map()函数接收两个参数，一个是函数，一个是Iterable，map将传入的函数依次作用到序列的每个元素，并把结果作为新的Iterator返回。

# def f(x):
#     return x * x
#
#
# r = map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9])
# print(list(r))


# endregion

# region Reduce 再看reduce的用法。reduce把一个函数作用在一个序列[x1, x2, x3, ...]上，这个函数必须接收两个参数，reduce把结果继续和序列的下一个元素做累积计算
#   reduce(f, [x1, x2, x3, x4]) = f(f(f(x1, x2), x3), x4)

# def add(x, y):
#     return x + y
#
#
# from functools import reduce
#
# print(reduce(add, [1, 2, 3, 4, 5]))
# endregion

# region Filter filter()函数用于过滤序列 filter()把传入的函数依次作用于每个元素，然后根据返回值是True还是False决定保留还是丢弃该元素。
# def is_odd(n):
#     return n % 2 == 1
#
#
# print(list(filter(is_odd, [1, 2, 4, 5, 6, 9, 10, 15])))
#
#
# def not_empty(s):
#     return s and s.strip()
#
#
# print(list(filter(not_empty, ['A', '', 'B', None, 'C', '  '])))
# endregion

#region Sort sort()按照Key的规则，从小到大排序
# print(sorted([36, 5, -12, 9, -21]))
# print(sorted([36, 5, -12, 9, -21],key=abs))
# print(sorted(['bob', 'about', 'Zoo', 'Credit']))
# print(sorted(['bob', 'about', 'Zoo', 'Credit'],key=str.lower))
# print(sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower, reverse=True))
#endregion

#region 返回函数
# def lazy_sum(*args):
#     def sum():
#         ax = 0
#         for n in args:
#             ax = ax + n
#         return ax
#     return sum
# f=lazy_sum(1,2,3,4,5)
# print (f)
# print(f())
#
# def count():
#     def f(j):
#         def g():
#             return j*j
#         return g
#     fs = []
#     for i in range(1, 4):
#         fs.append(f(i)) # f(i)立刻被执行，因此i的当前值被传入f()
#     return fs
#
# f1,f2,f3 =count()
# print(f1())
# print(f2())
# print(f2())
# print(f3())
#endregion

#region 匿名函数
# print(list(map(lambda x: x * x, [1, 2, 3, 4, 5, 6, 7, 8, 9])))
# f=lambda x:x*x
# print(f(5))
#endregion

#region 装饰器 本质上，decorator就是一个返回函数的高阶函数
def log(func):
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)
    return wrapper
@log
def now():
    print('2015-12-11')
print(now())
#endregion