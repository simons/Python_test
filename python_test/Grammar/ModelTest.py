#!/usr/bin/python
# FileName:Hello.py
class Hello(object):
    def hello(self, name='world'):
        print('Hello, %s.' % name)


def helloworld():
    print('Hello World')
