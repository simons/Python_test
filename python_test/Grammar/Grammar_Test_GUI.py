#!/usr/bin/env python
# encoding: utf-8

"""
@version: 3.6.3
@author: Zjay 
@license: Apache Licence 
@site: 
@software: PyCharm
@file: Grammar_Test_GUI.py
@time: 2017/12/3 16:05
"""

# region  GUI编程 （Tkinter库）
# from tkinter import *
# import tkinter.messagebox as messagebox
#
# # class Application(Frame):
# #     def __init__(self, master=None):
# #         Frame.__init__(self, master)
# #         self.pack()
# #         self.createWidgets()
# #
# #     def createWidgets(self):
# #         self.helloLabel = Label(self, text='Hello, world!')
# #         self.helloLabel.pack()
# #         self.quitButton = Button(self, text='Quit', command=self.quit)
# #         self.quitButton.pack()
#
# class Application(Frame):
#     def __init__(self, master=None):
#         Frame.__init__(self, master)
#         self.pack()
#         self.createWidgets()
#
#     def createWidgets(self):
#         self.nameInput = Entry(self)
#         self.nameInput.pack()
#         self.alertButton = Button(self, text='Hello', command=self.hello)
#         self.alertButton.pack()
#
#     def hello(self):
#         name = self.nameInput.get() or 'world'
#         messagebox.showinfo('Message', 'Hello, %s' % name)
#
# try:
#     app =Application()
#     # 设置窗口标题:
#     app.master.title('Hello World')
#     # 主消息循环:
#     app.mainloop()
# except BaseException as e:
#     print('Error:',e)
# finally:
#     print('End')
# endregion（）

# region GUI编程 （wxPython）
import wx

app = wx.App()
win = wx.Frame(None, title='test')
btnopen = wx.Button(win, label='open')
btnload = wx.Button(win, label='load')
win.Show()
app.MainLoop()

# endregion
