# region 使用模块
# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# testGrammar.py
' a test module '

# __author__ = 'Michael Liao'
#
# import sys
#
#
# def test():
#     args = sys.argv
#     if len(args) == 1:
#         print('Hello, world!')
#     elif len(args) == 2:
#         print('Hello, %s!' % args[1])
#     else:
#         print('Too many arguments!')
#
#
# if __name__ == '__main__':
#     test()
# endregion

# region 面向对象
# class Student(object):
#     def __init__(self, name, score):
#         self.name = name
#         self.score = score
#
#     def print_score(self):
#         print('{0},{1}'.format(self.name, self.score))
#         # print('%s: %s' % (self.name, self.score))
#
#
# bart = Student('Bart Simpson', 59)
# bart.print_score()
# endregion

# region 实体类定义
# class Student(object):
#     # def __init__(self, name, score):
#     def __init__(self, name, score):
#         self._name = name
#         self.score = score
#
#     def get_name(self):
#         return self._name
#
#     def set_name(self, name):
#         self._name = name
#
#     def print_score(self):
#         print('{0}:{1}'.format(self._name, self.score))
#
#     def get_grade(self):
#         if self.score > 80:
#             return 'A'
#         elif self.score > 60:
#             return 'B'
#         elif (self.score > 20):
#             return 'C'
#         else:
#             return 'D'
#
#
# lisa = Student('Lisa', 99)
# bart = Student('Bart', 59)
# print(lisa.get_grade())
# print(bart.get_grade())
# print(lisa._name)
# print(lisa.get_name())
# lisa.set_name('zhangsan')
# print(lisa._name)
# print(lisa.get_name())
# endregion

# region 继承和多态
# class Animal(object):
#     def run(self):
#         print('Animal is running...')
#
#
# class Dog(Animal):
#     def run(self):
#         print('dog  is running...')
#
#     pass
#
#
# class Cat(Animal):
#     def eat(self):
#         print('Cat  is eat...')
#         pass
#
#
# dog = Dog()
# dog.run();
# cat = Cat()
# cat.run()
# cat.eat()
# endregion

# region 获取对象类型
# print(type(123))
# print(type('123'))
# print(type(123.33))
# print(type([1,2,3]))
# print(type((1,2,3)))
# print(type(abs(-11)))
# def a():
#     pass
# print(type(a))
# print(isinstance(a,type(a)))
# print(isinstance(123,int))
# # dir() 得一个对象的所有属性和方法
# print(dir('ABC'))

# class Animal(object):
#     def __init__(self):
#         pass
#
#     def run(self):
#         print('Animal is running...')
#
#
# an = Animal()
# print(hasattr(an, 'run'))  # 有属性run吗？
# print(hasattr(an, 'x'))  # 有属性run吗？
# setattr(an, 'x', 19)  # 设置一个属性'x'
# print(an.x)
# print(hasattr(an, 'x'))  # 有属性x吗？
# print(getattr(an,'x')) # 获取x属性
# endregion

# region 实例属性和类属性
# class Student(object):
#     name = 'StudentName'
#
#     def __init__(self, age):
#         self.age = age
#
#
# def set_age(self, age):  # 单独定义一个方法
#     self.age = age
#
#
# stu = Student(12)
# print('{0}:{1}'.format(stu.name, stu.age))
# # 实体类添加方法
# from types import MethodType
#
# stu.set_age = MethodType(set_age, stu)  # 给实例绑定一个方法,只能该实例使用
# stu.set_age(25)
# print('{0}:{1}'.format(stu.name, stu.age))
# Student.set_age = MethodType(set_age, stu)  # 给一个实体类绑定一个方法，该实体类实例化的所有实例都可以使用
# stunew = Student(13)
# stunew.set_age(26)
# print('{0}:{1}'.format(stunew.name, stunew.age))
# endregion

# region  使用 __slots__ 限制绑定属性
# class Student(object):
#     __slots__ = ('name', 'age')  # 用tuple定义允许绑定的属性名称
#
#
# try:
#     s = Student()
#     s.name = 'mic'
#     s.age = 12
#     s.score = 99
#
# except BaseException as e:
#     print('Error:{0}'.format(e))
# finally:
#     if hasattr(s, 'score'):
#         print('{0}，{1},{2}'.format(s.name, s.age, s.score))
#     else:
#         print('{0}，{1}'.format(s.name, s.age))
#     print('End')
# endregion

# region  @property装饰器就是负责把一个方法变成属性调用的
# class Student(object):
#     @property
#     def score(self):
#         return self._score
#
#     @score.setter
#     def score(self, value):
#         if not isinstance(value, int):
#             raise ValueError('score must be an integer!')
#         if value < 0 or value > 100:
#             raise ValueError('score must between 0 ~ 100!')
#         self._score = value
#
#     @score.getter
#     def score(self):
#         return self._score * 1.1
#
#
# try:
#     stu = Student()
#     stu.score = 60
#     print(stu.score)
#     stu.score = 999
#     print(stu.score)
# except BaseException as ex:
#     print('Error :{0}'.format(ex))


# endregion

# region  定制类(可以多看看)
# class Student(object):
#     def __init__(self, name):
#         self.name = name
#
#     def __str__(self):  # 打印实例文字
#         return 'Student object (name: %s)' % self.name
#
#
# print(Student('mic'))
#
#
# class Fib(object):
#     def __init__(self):
#         self.a, self.b = 0, 1  # 初始化两个计数器a，b
#
#     def __iter__(self):
#         return self  # 实例本身就是迭代对象，故返回自己
#
#     def __next__(self):
#         self.a, self.b = self.b, self.a + self.b  # 计算下一个值
#         if self.a > 100:  # 退出循环的条件
#             raise StopIteration()
#         return self.a  # 返回下一个值
#
#
# for n in Fib():
#     print(n)
#
#
# class Fib(object):
#     def __getitem__(self, n):
#         if isinstance(n, int): # n是索引
#             a, b = 1, 1
#             for x in range(n):
#                 a, b = b, a + b
#             return a
#         if isinstance(n, slice): # n是切片
#             start = n.start
#             stop = n.stop
#             if start is None:
#                 start = 0
#             a, b = 1, 1
#             L = []
#             for x in range(stop):
#                 if x >= start:
#                     L.append(a)
#                 a, b = b, a + b
#             return L
#
# f = Fib()
# print(f[0:5])
# endregion

# region 枚举类
# from enum import Enum
#
# Month = Enum('Month', ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))
# for name, member in Month.__members__.items():
#     print(name, '=>', member, ',', member.value)
#
#
# class Gender(Enum):
#     Male = 0
#     Female = 1
#
#
# class Student(object):
#     def __init__(self, name, gender):
#         self.name = name
#         self.gender = gender
#
#
# bart = Student('bart', Gender.Female)
# if bart.gender == Gender.Female:
#     print('测试通过!')
# else:
#     print('测试失败!')
# endregion

# region 元类
#import Hello.py

# endregion
