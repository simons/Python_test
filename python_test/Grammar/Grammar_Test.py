# region基础语法
# #字符，表达式
# str = 'hello world';
# print(str);
# # test 变量
# a = 'ABC'
# b = a
# a = 'XYZ'
# print(b)
# # test
# print('\u4e2d\u6587');
# print(ord('A'));
# print(chr(65));
# x = b'ABC';
# print( 'ABC'.encode('ascii'));
# print( '中文'.encode('utf-8')); #b'\xe4\xb8\xad\xe6\x96\x87'
# print( b'\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8'));
# print(len('ABC'));print('+');print(len('中文'));
# print(len(b'\xe4\xb8\xad\xe6\x96\x87'))
# print('hello ,%s,%d,%f'%('world',1233,4.444));
# print('hello ,{0},{1},{2}'.format('world',1233,4.444));
# endregion

# region List 集合
# classmates = ['Michael', 'Bob', 'Tracy']
# print(classmates);
# print(len(classmates));
# print(classmates[0])
# print(classmates[2])
# print(classmates[-1])
# print(classmates[-3])
# classmates.append(['Tom','lucy'])
# print(classmates);
# classmates.pop(0);#默认移除最后一个，若指定了index ,则移除index 位置的值
# print(classmates);
# classmates.insert(0,'Test')
# print(classmates);
# print(classmates.index('Test'))
# endregion

# region 有序列表叫元组：tuple
# classmates = ('Michael', 'Bob', 'Tracy')
# print(classmates);
# t = ('a', 'b', ['A', 'B'])
# print(t);
# t[2][0] = 'X'
# t[2][1] = 'Y'
# print(t);
# endregion

# region if判断
# age = 3
# if age >= 18:
#     print('your age is', age)
#     print('adult')
# else:
#     print('your age is', age)
#     print('teenager')
# if age >= 18:
#     print('adult')
# elif age >= 6:
#     print('teenager')
# else:
#     print('kid')
# if age:
#     print(True)
#     print('input birth ')
# birth = int(input())
# if birth < 2000:
#     print('00前')
# else:
#     print('00后')
# endregion

# region for 循环
# classmates = ['Michael', 'Bob', 'Tracy']
# for item in classmates:
#     print(item);
# sum = 0
# for x in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
#     sum = sum + x
# print(sum)
# print(list(range(5)))
# i = 0
# while i < len(classmates):
#     print(classmates[i])
#     i = i + 1;
#
# n = 0
# while n < 10:
#     n = n + 1
#     if n % 2 == 0: # 如果n是偶数，执行continue语句
#         print(n)
#         continue # continue语句会直接继续下一轮循环，后续的print()语句不会执行
#    # print(n)

# endregion

# region Dictionary 字典
# d = {'Michael': 95, 'Bob': ['Bob1','Bob2'], 'Tracy': 85} #查找速度快，占用内存高
# print(d['Michael']);
# print(d['Bob']);
# d['Tom']=100;
# print(d);
# print('Bob' in d)
# print(d.get('Bob'))
# print(d.get('abs'))
# d.pop('Bob')
# print(d)
# endregion

# region Set 集合 自动去重
# s = set([1, 1, 2, 2, 3, 3])
# print(s)
# s.add(4)
# print(s)
# s.remove(4)
# print(s);
# s1 = set([1, 2, 3])
# s2 = set([2, 3, 4])
# print(s1 & s2)
# print(s1 | s2)
# endregion

# region 函数调用
# print(abs(-20))
# print(max(2, 3, 1, -5))
# print(min(2, 3, 1, -5))
# print(int('1234'))
# print( float('12.34'))
# print(str(1.23))
# print(bool(1))
# print(bool(''))
#
# a=abs # 变量a指向abs函数
# print(a(-20))  # 所以也可以通过a调用abs函数
# endregion

# region 自定义函数及调用
# def my_abs1(x):
#     if x >= 0:
#         return x
#     else:
#         return -x
#
#
# print(my_abs1(-20))
#
#
# def my_abs2(x):
#     if not isinstance(x, (int, float)):
#         #raise TypeError('bad operand type')
#         a =float(x)
#     else :
#         a=x
#     if a > 0:
#         return a
#     else:
#         return -a
#
#
# print(my_abs2(input()))
# import math;
#
#
# def move(x, y, step, angle=0):
#     nx = x + step * math.cos(angle)
#     ny = y - step * math.sin(angle)
#     return nx, ny
#
#
# x, y = move(100, 100, 60, math.pi / 6)
# print(x, y);
# endregion

# region 自定义函数可变参数
# def power(x, n=2):
#     s = 1
#     while n > 0:
#         n = n - 1
#         s = s * x
#     return s
#
#
# print(power(5, 3))
# print(power(5))
#
#
# def calc1(numbers):
#     sum = 0
#     for n in numbers:
#         sum = sum + n * n
#     return sum
#
#
# print(calc1([1, 2, 3]))
# print(calc1((1, 3, 5, 7)))
#
#
# def calc2(*numbers):
#     sum = 0
#     for n in numbers:
#         sum = sum + n * n
#     return sum
#
#
# print(calc2(1, 2, 3))
# print(calc2(1, 3, 5, 7))
# num = [1, 2, 3]
# print(calc2(num[0], num[1], num[2]))
# print(calc2(*num))
#
# def person1(name, age, **kw):
#     print('name:', name, 'age:', age, 'other:', kw)
#
# def person2(name, age,  city,*args, job):
#      print(name, age, city, job)
# person1('Bob', 35, city='Beijing')
# extra = {'city': 'Beijing', 'job': 'Engineer'}
# person1('Jack', 24, city=extra['city'], job=extra['job'])
# person2('Jack', 24, city='Beijing', job='Engineer')
# person2('Jack', 24, 'Beijing', job='Engineer')
#
# def f1(a, b, c=0, *args, **kw):
#     print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw);
# f1(1, 2, '' ,'a', 'b')
# f1(1, 2, 3, 'a', 'b', x=99)
# endregion

# region 递归函数(函数的自调用)
# def fact(n):
#     print(n)
#     if n==1:
#         return 1
#     return n * fact(n - 1)
# print(fact(3))
# endregion

# region 切片特性（Slice）
# L = ['Michael', 'Sarah', 'Tracy', 'Bob', 'Jack']
# print(L[0:3])
# print(L[:3])
# print(L[2:3])
# L = list(range(100))
# print(L[10:20])
# print(L[:10:2])
# print(L[::5])
# endregion

# region 迭代
# d = {'a': 1, 'b': 2, 'c': 3}
# for kvp in d.values() :
#     print(kvp)
# for kvp in d.keys():
#     print(kvp)
# from collections import Iterable
# print(isinstance('abc', Iterable))
# for x, y in [(1, 1), (2, 4), (3, 9)]:
#     print(x,y)
# endregion

# region 列表生成式
# print([x * x for x in range(1, 11)])
# print( [x * x for x in range(1, 11) if x % 2 == 0])
# print([m + n for m in 'ABC' for n in 'XYZ'])
# import os # 导入os模块，模块的概念后面讲到
# print([d for d in os.listdir('.')]) # os.listdir可以列出文件和目录
# d = {'x': 'A', 'y': 'B', 'z': 'C' }
# for k,v in d.items() :
#     print(k,"=",v)
# L = ['Hello', 'World', 'IBM', 'Apple']
# print( [s.lower() for s in L])
# endregion

# region 生成器(一边循环一边计算的机制)

# try:
#     L = [x * x for x in range(10) if x % 2 == 0]  # list
#     print(L)
#     g = (x for x in range(10))  # generator
#     print(g)
#     for x in g:
#         print("for G :", x)
#     print(next(g))
#     print(next(g))
#     print(next(g))
#     print(next(g))
#     for x in g:
#         print("for G1 :", x)
# except StopIteration as ex:
#     print(ex)
# else:
#     print('no error!')

# endregion

# region 异常
try:
    print('try...')
    r = 10 / int('a')
    print('result:', r)
except ValueError as e:
    print('ValueError:', e)
except ZeroDivisionError as e:
    print('ZeroDivisionError:', e)
except BaseException as e:
    print('BaseException:', e)
finally:
    print('finally...')
print('END')
# endregion
