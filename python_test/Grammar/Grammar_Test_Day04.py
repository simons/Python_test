# region 自定义库的使用，在调用之前需要告知解释器，文件存放位置
# import sys
#
# try:
#     sys.path.append('D:/Project/Project_Git/Python_test/python_test/Grammar')  # 添加解释器调用模块路径
#
#     import ModelTest
#
#     print(ModelTest)
#
#     ModelTest.helloworld()
# except BaseException as e:
#     print(e)

# endregion

# region 标准库使用（官方）
# import sys
#
# path = 'D:\Project\Project_Git\Python.Framwork\Python.Framwork'
# if path in sys.path:
#     print("contains path")
# else:
#     sys.path.append('D:\Project\Project_Git\Python.Framwork\Python.Framwork')
# print(sys.path)
# endregion

# region 文件读写
# try:
#     f = open('D:\Project\Project_Git\Python_test\python_test\Comm\\test.txt', 'r')
#     str = f.readline()
#     print(str)
#     content = f.read()
#     print(content)
#     content = 'test file read and write'
#     print(f.writable())
#     # f.write(content)
#     f.close()
# except BaseException as ex:
#     print('open file failure ', ex)
# finally:
#     print('End')

# endregion

# region 内存读写 StringIO
# from io import StringIO
# from io import BytesIO
#
# try:
# f = StringIO()
# f.write('hello')
# f.write(',')
# f.write('world')
# print(f.getvalue())

# fs = StringIO('Hello!\\nHi!\\nGoodbye!')
# for n in fs.readline():
#     if n == '':
#         break
#     else:
#         print(n)

# f = BytesIO()
# f.write('中文'.encode('utf-8'))
# print(f.getvalue())

# except BaseException as ex:
#     print('Error', ex)
# endregion

# region 文件目录操作
# import os
#
# try:
#     if os.name == 'posix':
#         print('OS is Linux or Unix or Mac OS X')
#     else:
#         print('OS is Windows')
#         # print(os.uname())
#         #os.environ()
#         print(os.path.abspath('.'))#获取绝对路径
#         newpath= os.path.join(os.path.abspath(''),'test')
#         ##if os._exists(newpath)
#         #os.mkdir(newpath)
#         os.rmdir(newpath)
# except BaseException as ex:
#     print('Error:', ex)
# endregion
